import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessagesfComponent } from './messagesf.component';

describe('MessagesfComponent', () => {
  let component: MessagesfComponent;
  let fixture: ComponentFixture<MessagesfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessagesfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessagesfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
