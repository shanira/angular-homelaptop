import { MessagesService } from './../messages/messages.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'messagesf',
  templateUrl: './messagesf.component.html',
  styleUrls: ['./messagesf.component.css']
})
export class MessagesfComponent implements OnInit {

  messages;

  //only indepense injection
  constructor(private service:MessagesService) { }

  //רוצים למשוך את הרשימה שהקומפוננט נוצר
  //פונקציה שמתעוררת ברגע שהקומפוננט נוצר לאחר הקונסטרקטור
  ngOnInit() {
    this.service.getMessagesFire().subscribe(response=>{
      console.log(response);
      this.messages = response;
    });
  }

}