import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { HttpParams} from '@angular/common/http';
import { AngularFireDatabase } from 'angularfire2/database';

import 'rxjs/Rx';

@Injectable()
export class MessagesService {

  //שמאל תכונה, ימין סוג המחלקה
  http:Http; //המחלקה מנהלת את התקשורת עם השרת, ספציפי עם הרסט איפיאי

  getMessages(){
   // return ['Message 1','Message 2' ,'Message 3' ,'Message 4'];
   //get messages from the SLIM rest API (No DB)

   //return this.http.get('http://localhost/slim/messages', options);
     return this.http.get(environment.url + 'messages');
  }

  //השרת שמתחבר לפיירבייס
  getMessagesFire(){
    //הוויליו מייצר את האובזווריבל
    return this.db.list('/messages').valueChanges();
   }

   //ביצוע עדכון של הודעה
   putMessage(data,key){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('message',data.message);
    return this.http.put('http://localhost/slim/messages/'+ key,params.toString(), options);
  }

  postMessage(data){
    //המרת גייסון למפתח וערך
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }

    let params = new HttpParams().append('message',data.message);
    
    //return this.http.post('http://localhost/slim/messages', params.toString(), options);
   
    return this.http.post(environment.url + 'messages', params.toString(), options);
     
  }
  deleteMessage(key){
    //return this.http.delete('http://localhost/slim/messages/'+ key);
    return this.http.delete(environment.url + 'messages/'+ key);

  }

   //הודעה בודדת
  getMessage(id){
     //return this.http.get('http://localhost/slim/messages/'+ id);
    return this.http.get(environment.url + 'messages/'+ id);

  }
  //בתוך הקונסטרקטור נוצר מופע של האובייקט, דפנדיסי אינג'קטיון
  constructor(http:Http, private db:AngularFireDatabase) {
    this.http=http; 
  }
  

}
