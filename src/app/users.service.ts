import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {HttpParams} from '@angular/common/http';

@Injectable()
export class UsersService {
  http:Http;
  getUsers(){
    //return ['a','b','c'];
    //get users from the SLIM rest API (Don't say DB)
    return  this.http.get('http://localhost/slim/users');
  }

  getUser(id){
    return  this.http.get('http://localhost/slim/users/'+id);
  }

  postUser (data){//input:Json, output: (key,value) //send the data to the server
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('user',data.user)
    return this.http.post('http://localhost/slim/users', params.toString(), options);
  }

  deleteUser (key){
    return this.http.delete('http://localhost/slim/users/'+key)
  }

  constructor(http:Http) { 
    this.http = http;
  }
}