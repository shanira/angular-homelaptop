import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';//
import { MessagesService } from './messages/messages.service';//
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { MessagesComponent } from './messages/messages.component';
import { MessagesFormsComponent } from './messages-forms/messages-forms.component';
import { NavigationComponent } from './navigation/navigation.component';
import { UsersComponent } from './users/users.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { MessageComponent } from './message/message.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { MessagesfComponent } from './messagesf/messagesf.component';
//import { environment } from './../environments/environment.prod';
import { environment } from './../environments/environment';
import { UsersService } from './users.service';
import { MessageFormComponent } from './message-form/message-form.component';



@NgModule({
  declarations: [
    AppComponent,
    MessagesComponent,
    MessagesFormsComponent,
    NavigationComponent,
    UsersComponent,
    NotFoundComponent,
    MessageComponent,
    MessagesfComponent,
    MessageFormComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    RouterModule.forRoot([
    {path: '', component: MessagesComponent},
    {path: 'users', component: UsersComponent},
    {path: 'message/:id', component: MessageComponent},
    {path: 'messagesf', component: MessagesfComponent},
     {path: '**', component: NotFoundComponent}
          ])
         ],
  providers: [
  MessagesService,
  UsersService

],
  bootstrap: [AppComponent]
})
export class AppModule { }
