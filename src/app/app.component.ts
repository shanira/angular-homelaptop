import { Component } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from "angularfire2/database-deprecated";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
    
  items: FirebaseListObservable<any[]>;
  msg: string = '';


  send(chatMsg: string) {
    this.items.push({ message: chatMsg });
    this.msg = '';
  }

}
