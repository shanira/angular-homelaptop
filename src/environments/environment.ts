// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

//משתנה קבוע שהאפליקציה לא יכולה לשנות את הערכים שלו, משמש למטרה שהאפליקציה אמורה לעבוד בסביבות שונות
//בסביבת פיתוח נעבור מול השרת (סלים) בשונה מסביבת טסט שעובדת מקומית
//למשל כדי לדעת להבדיל בין הסביבות נשים אייקון של טסט, בעזרת המשתנה הנ"ל אנו נוכל להגדיר אם הוא יופיע או לא

//הגדרה מקומית localhost
export const environment = {
  production: false,
  url: 'http://localhost/slim/',
  firebase:{
    apiKey: "AIzaSyDjNS1hBpChoyW_GwMWVuBNY12x9_U72CY",
    authDomain: "messages-d6d61.firebaseapp.com",
    databaseURL: "https://messages-d6d61.firebaseio.com",
    projectId: "messages-d6d61",
    storageBucket: "messages-d6d61.appspot.com",
    messagingSenderId: "574686788959"
  }
};